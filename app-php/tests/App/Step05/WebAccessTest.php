<?php

namespace App\Step05;

use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;

class WebAccessTest extends TestCase
{
    const BASE_URI = 'http://localhost:8000';
    private $httpClient;

    /**
     * @before
     */
    public function init()
    {
        $this->httpClient = new Client([
            'base_uri'    => self::BASE_URI,
            'http_errors' => false,
        ]);
    }

    /**
     * @test
     */
    public function should_return_article_list()
    {
        $response = $this->httpClient->request('GET', '/');

        $contentType = $response->getHeader('content-type')[0];
        $body        = json_decode($response->getBody()->getContents());

        $data = json_decode($this->getTrimmedJsonFromFile(__DIR__ . '/../../../resources/articles-list.json'));
        self::assertThat($contentType, self::equalTo('application/json'));
        self::assertThat($body, self::equalTo($data));
    }

    private function getTrimmedJsonFromFile(string $path): string
    {
        return json_encode(json_decode(file_get_contents($path)), JSON_UNESCAPED_SLASHES);
    }
}