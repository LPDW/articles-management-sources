<?php

namespace App\Step07;

use PHPUnit\Framework\TestCase;

class RegExpTest extends TestCase
{
    const PATTERN = '/^\/article\/(\d+)$/';

    /**
     * @test
     */
    public function preg_match_valid_path()
    {
        $result = preg_match(self::PATTERN, '/article/2', $matches);
        self::assertThat($result, self::equalTo(1));
        self::assertThat($matches[0], self::equalTo('/article/2'));
        self::assertThat($matches[1], self::equalTo(2));
    }

    /**
     * @test
     */
    public function preg_match_valid_path_with_2_digits()
    {
        $result = preg_match(self::PATTERN, '/article/22', $matches);
        self::assertThat($result, self::equalTo(1));
        self::assertThat($matches[0], self::equalTo('/article/22'));
        self::assertThat($matches[1], self::equalTo(22));
    }

    /**
     * @test
     */
    public function preg_match_fails_with_invalid_id()
    {
        $result = preg_match(self::PATTERN, '/article/a', $matches);
        self::assertThat($result, self::equalTo(0));
        self::assertThat($matches, self::isEmpty());
    }

    /**
     * @test
     */
    public function preg_match_fails_with_no_id()
    {
        $result = preg_match(self::PATTERN, '/article/', $matches);
        self::assertThat($result, self::equalTo(0));
        self::assertThat($matches, self::isEmpty());
    }

    /**
     * @test
     */
    public function preg_match_fails_with_invalid_path()
    {
        $result = preg_match(self::PATTERN, '/test/article/2', $matches);
        self::assertThat($result, self::equalTo(0));
        self::assertThat($matches, self::isEmpty());
    }
}