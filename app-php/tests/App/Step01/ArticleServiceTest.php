<?php

namespace App\Step01;

use App\ArticleService;
use App\UnknownArticleException;
use PHPUnit\Framework\TestCase;

class ArticleServiceTest extends TestCase
{
    /**
     * @test
     */
    public function should_list_articles_as_json()
    {
        $service = new ArticleService();

        $articleList = $service->list();

        self::assertThat($articleList, self::equalTo(file_get_contents(__DIR__ . '/../../../resources/articles-list.json')));
    }

    /**
     * @test
     */
    public function should_find_article_by_id()
    {
        $service = new ArticleService();

        $article = $service->get(1);

        self::assertThat($article,
            self::equalTo(file_get_contents(__DIR__ . '/../../../resources/article-1.json')));
    }

    /**
     * @test
     */
    public function should_throw_exception_for_unknown_article_id()
    {
        $service = new ArticleService();

        $this->expectException(UnknownArticleException::class);
        $this->expectExceptionMessage('Unknown article');
        $service->get(100);
    }
}
